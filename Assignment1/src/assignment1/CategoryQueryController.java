// Nicolas Abelanet
// Friday, October 22nd
// CategoryQueryController.java

package assignment1;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CategoryQueryController extends QueryController<Category> {

    // The query for getting all categories.
    private static final String get_categories_query = "select TYPE from HENRY_BOOK group by TYPE";

    public CategoryQueryController(DatabaseManager database_manager) {
        super(database_manager);
    }

    // Gets data from the database. This is a master controller so the key will always be null.
    @Override
    public ArrayList<Category> getData(DatabaseObject key) throws SQLException {
        return parseData(database_manager.executeQuery(get_categories_query));
    }

    @Override
    public ArrayList<Category> parseData(ResultSet result_set) throws SQLException {
        return parseCategories(result_set);
    }

    // Parses a result set into an array list of categories.
    private ArrayList<Category> parseCategories(ResultSet result_set) throws SQLException {

        ArrayList<Category> categories = new ArrayList<>();

        // Gets the type from the rows returned from the database.
        while (result_set.next()) {
            String type = result_set.getString("TYPE");

            Category new_category = new Category(type);
            categories.add(new_category);
        }

        return categories;

    }

}
