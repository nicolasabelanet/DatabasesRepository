// Nicolas Abelanet
// Friday, October 22nd
// PublisherQueryController.java

package assignment1;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PublisherQueryController extends QueryController<Publisher> {

    // The query for getting all publishers.
    private static final String get_publishers_query = "select PUBLISHER_NAME, PUBLISHER_CODE from HENRY_PUBLISHER";

    public PublisherQueryController(DatabaseManager database_manager) {
        super(database_manager);
    }

    // Gets data from the database. This is a master controller so the key will always be null.
    @Override
    public ArrayList<Publisher> getData(DatabaseObject key) throws SQLException {
        return parseData(database_manager.executeQuery(get_publishers_query));
    }

    // Parses the data received from the database.
    @Override
    public ArrayList<Publisher> parseData(ResultSet result_set) throws SQLException {
        return parsePublishers(result_set);
    }

    // Parses a result set into an array list of publishers.
    private ArrayList<Publisher> parsePublishers(ResultSet result_set) throws SQLException {
        ArrayList<Publisher> publishers = new ArrayList<>();

        // Gets the Publisher name and code from the rows returned from the database.
        while (result_set.next()) {
            String publisher_name = result_set.getString("PUBLISHER_NAME");
            String publisher_code = result_set.getString("PUBLISHER_CODE");

            Publisher new_publisher = new Publisher(publisher_name, publisher_code);
            publishers.add(new_publisher);
        }

        return publishers;
    }




}
