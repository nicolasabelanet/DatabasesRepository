// Nicolas Abelanet
// Friday, October 22nd
// Author.java

package assignment1;

public class Author extends DatabaseObject {

    private final String author_first; // The string first name of the author.
    private final String author_last; // The string last name of the author.
    private final int author_num; // The ID of an author.

    public Author (String author_first, String author_last, int author_num) {
        this.author_first = author_first;
        this.author_last = author_last;
        this.author_num = author_num;
    }

    @Override
    public String toString() {
        return author_first + " " + author_last;
    }

    public int getAuthorNum() {
        return author_num;
    }

}
