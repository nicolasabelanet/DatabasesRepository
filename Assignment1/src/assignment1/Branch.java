// Nicolas Abelanet
// Friday, October 22nd
// Branch.java

package assignment1;

public class Branch extends DatabaseObject {

    private final String branch_name; // The string name of the branch that has a specific book.
    private final int on_hand; // The number of a specific book present at a branch.

    public Branch (String branch_name, int on_hand) {
        this.branch_name = branch_name;
        this.on_hand = on_hand;
    }

    @Override
    public String toString() {
        return (branch_name + ": " + on_hand);
    }


}
