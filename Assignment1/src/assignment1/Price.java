// Nicolas Abelanet
// Friday, October 22nd
// Price.java

package assignment1;

import java.text.NumberFormat;

public class Price extends DatabaseObject {

    private final double price; // The price of a book.

    public Price (double price) {
        this.price = price;
    }

    // Formats the double price to currency format.
    @Override
    public String toString () {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(price);
    }

}
