// Nicolas Abelanet
// Friday, October 22nd
// ListController.java

package assignment1;

import javax.swing.*;

public class ListController <T> extends DataDisplayController {

    private final JList<T> list; // The list that will be displayed.
    private final UpdateController<T[]> update_controller; // The object that will be asked how to handle an update.

    public ListController(UpdateController<T[]> update_controller) {
        super(false);
        this.update_controller = update_controller;
        list = new JList<>();
    }

    public JList<T> getList() {
        return list;
    }

    // Asks the update_controller how to handle an update.
    public void update (DatabaseObject update) {

        list.removeAll();

        if (update == null) {
            Debug.log("ListController received update: (null)");
        }
        else {

            Debug.log("ListController received update: " + "(" + update.getClass().getSimpleName() + ") " + update);

            try {
                list.setListData(update_controller.handleUpdate(update));
            } catch (Exception e) {
                Debug.log(e.getMessage());
                e.printStackTrace();
            }

        }

    }


}
