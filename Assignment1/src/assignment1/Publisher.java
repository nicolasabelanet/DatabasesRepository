// Nicolas Abelanet
// Friday, October 22nd
// Publisher.java

package assignment1;

public class Publisher extends DatabaseObject {

    private final String publisher_name; // The string name of the publishers entire name.
    private final String publisher_code; // The publisher code or id.

    public Publisher(String publisher_name, String publisher_code) {
        this.publisher_name = publisher_name;
        this.publisher_code = publisher_code;
    }

    public String getPublisherCode() {
        return publisher_code;
    }

    @Override
    public String toString() {
        return publisher_name;
    }
}
