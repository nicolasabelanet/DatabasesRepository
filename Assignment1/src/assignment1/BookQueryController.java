// Nicolas Abelanet
// Friday, October 22nd
// BookQueryController.java

package assignment1;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookQueryController extends QueryController<Book> {

    // The different queries for getting books from different types of keys.
    private static final String get_books_from_authors_query = "select TITLE, b.BOOK_CODE, PRICE from HENRY_BOOK b join HENRY_WROTE w on b.BOOK_CODE = w.BOOK_CODE where w.AUTHOR_NUM = ";
    private static final String get_books_from_category_query = "select TITLE, BOOK_CODE, PRICE from HENRY_BOOK where TYPE = ";
    private static final String get_books_from_publisher_query = "select TITLE, BOOK_CODE, PRICE from HENRY_BOOK where PUBLISHER_CODE = ";
    private static final String get_inventory_from_books_query =  "select b.BRANCH_NAME, i.ON_HAND from HENRY_BRANCH b join HENRY_INVENTORY i on b.BRANCH_NUM = i.BRANCH_NUM where i.BOOK_CODE = ";

    public BookQueryController(DatabaseManager database_manager) {
        super(database_manager);
    }

    // Handles getting data from the database. A list of books can be received from
    // multiple different types of keys.
    @Override
    public ArrayList<Book> getData(DatabaseObject key) throws SQLException {

        if (key.getClass() == Author.class) {
           return parseData(database_manager.executeQuery(get_books_from_authors_query + ((Author) key).getAuthorNum()));
        }
        else if (key.getClass() == Category.class) {
            return parseData(database_manager.executeQuery(get_books_from_category_query + "'" + ((Category) key).getType() + "'"));
        }
        else if (key.getClass() == Publisher.class) {
            return parseData(database_manager.executeQuery(get_books_from_publisher_query + "'" + ((Publisher) key).getPublisherCode() + "'"));
        }

        throw new SQLException();

    }

    @Override
    public ArrayList<Book> parseData(ResultSet result_set) throws SQLException {
        return parseBooks(result_set);
    }

    // Parses a result set into an array list of books.
    private ArrayList<Book> parseBooks(ResultSet result_set) throws SQLException {

        ArrayList<Book> books = new ArrayList<>();

        // Gets the Book title, book code and price from the
        // rows returned from the database.
        while (result_set.next()) {

            String title = result_set.getString("TITLE");
            String book_code = result_set.getString("BOOK_CODE");
            double price = result_set.getDouble("PRICE");

            // Gets the inventory from a specific book code.
            ArrayList<Branch> inventory = parseInventory(database_manager.executeQuery(get_inventory_from_books_query + "'" + book_code + "'"));

            Book new_book = new Book(title, new Price(price), inventory);
            books.add(new_book);

        }

        return books;

    }

    // Parses a result set into an array list of branches as an inventory.
    private ArrayList<Branch> parseInventory(ResultSet result_set) throws SQLException {

        ArrayList<Branch> inventory = new ArrayList<>();

        // Gets the branch name and inventory information from the
        // rows returned from the database.
        while (result_set.next()) {
            String branch_name = result_set.getString("BRANCH_NAME");
            int on_hand = result_set.getInt("ON_HAND");
            Branch new_branch = new Branch(branch_name, on_hand);
            inventory.add(new_branch);
        }

        return inventory;

    }


}
