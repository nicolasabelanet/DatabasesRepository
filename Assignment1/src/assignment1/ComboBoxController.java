// Nicolas Abelanet
// Friday, October 22nd
// ComboBoxController.java

package assignment1;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class ComboBoxController<T> extends DataDisplayController {

    private final JComboBox<T> combo_box; // The combobox that will be displayed.
    private final QueryController<T> query_controller; // The object responsible for getting data from the database.
    public ComboBoxController(QueryController<T> query_controller, boolean is_master) throws SQLException {

        super(is_master);

        this.query_controller = query_controller;

        combo_box = new JComboBox<>();
        combo_box.removeAllItems();

        // If this is a master data display object then
        // preemptively get data from the database.
        if (is_master) {

           ArrayList<T> database_objects = query_controller.getData();

           for (T data : database_objects) {
                combo_box.addItem(data);
           }

        }

        // If this combo box updates cascade that update to all its children.
        combo_box.addActionListener((event) -> {

            DatabaseObject selected = (DatabaseObject) combo_box.getSelectedItem();

            if (selected != null) {

                Debug.log("ComboBoxController selected: (" + selected.getClass().getSimpleName() + ") " + selected);

                try {
                    updateChildren(selected);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

        });

    }

    // Add a child to the list of children.
    public void addChild(DataDisplayController dc) {

        super.addChild(dc);

        try {
            updateChildren((DatabaseObject)getComboBox().getSelectedItem());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public JComboBox<T> getComboBox() {
        return combo_box;
    }

    // If an update is received ask the query controller
    // what do with that update.
    @Override
    public void update (DatabaseObject update) throws SQLException {

        Debug.log("ComboBoxController received update: " + "(" + update.getClass().getSimpleName() + ") " + update);

        combo_box.removeAllItems();

        ArrayList<T> database_objects = query_controller.getData(update);

        for (T data : database_objects) {
            combo_box.addItem(data);
        }

    }

}
