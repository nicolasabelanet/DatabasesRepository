// Nicolas Abelanet
// Friday, October 22nd
// Category.java

package assignment1;

public class Category extends DatabaseObject {

    private final String type; // The category code or type of category.

    public Category(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }

}
