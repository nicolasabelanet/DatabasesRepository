// Nicolas Abelanet
// Friday, October 22nd
// PriceUpdateController.java

package assignment1;

public class PriceUpdateController extends UpdateController<Price> {

    @Override
    // Handles an update sent by a parent
    public Price handleUpdate(DatabaseObject update) throws Exception {

        if (update.getClass() == Book.class) {
            return ((Book) update).getPrice();
        }

        throw new Exception("Handling update of type " + update.getClass().getSimpleName() + " is not implemented");

    }

}
