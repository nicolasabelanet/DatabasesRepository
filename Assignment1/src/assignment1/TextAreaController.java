// Nicolas Abelanet
// Friday, October 22nd
// TextAreaController.java

package assignment1;

import javax.swing.*;

public class TextAreaController<T> extends DataDisplayController {

    private final JTextArea text_area; // The text area that will be displayed.
    private final UpdateController<T> updateController; // The object that will be asked how to handle an update.

    public TextAreaController(UpdateController<T> update_controller)  {

        super(false);

        this.updateController = update_controller;

        text_area = new JTextArea();
        text_area.setEditable(false);

    }

    public JTextArea getTextArea() {
        return text_area;
    }

    // Asks the update_controller how to handle an update.
    public void update (DatabaseObject update)  {

        text_area.setText("");

        if (update == null) {

            Debug.log("TextAreaController received update: (null)");

            text_area.setText("N/A");

        }

        else {

            Debug.log("TextAreaController received update: " + "(" + update.getClass().getSimpleName() + ") " + update);

            try {
                text_area.setText(updateController.handleUpdate(update).toString());
            } catch (Exception e) {
                Debug.log(e.getMessage());
                e.printStackTrace();
            }

        }

    }


}
