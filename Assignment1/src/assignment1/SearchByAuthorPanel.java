// Nicolas Abelanet
// Friday, October 22nd
// SearchByAuthorPanel.java

package assignment1;

import javax.swing.*;
import java.sql.SQLException;

public class SearchByAuthorPanel extends JPanel {

    public SearchByAuthorPanel(DatabaseManager database_manager) throws SQLException {

        AuthorQueryController author_query_controller = new AuthorQueryController(database_manager);
        BookQueryController book_query_controller = new BookQueryController(database_manager);
        PriceUpdateController price_update_controller = new PriceUpdateController();
        InventoryUpdateController inventory_update_controller = new InventoryUpdateController();

        ComboBoxController<Author> author_box_controller = new ComboBoxController<>(author_query_controller, true);
        ComboBoxController<Book> book_box_controller = new ComboBoxController<>(book_query_controller, false);
        TextAreaController<Price> price_controller = new TextAreaController<>(price_update_controller);
        ListController<Branch> inventory_controller = new ListController<>(inventory_update_controller);

        author_box_controller.addChild(book_box_controller);
        book_box_controller.addChild(price_controller);
        book_box_controller.addChild(inventory_controller);

        this.add(author_box_controller.getComboBox());
        this.add(book_box_controller.getComboBox());
        this.add(price_controller.getTextArea());
        this.add(inventory_controller.getList());

    }

}
