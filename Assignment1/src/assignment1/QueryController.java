// Nicolas Abelanet
// Friday, October 22nd
// QueryController.java

package assignment1;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class QueryController <T> {

    protected DatabaseManager database_manager; // The database that will be queried.

    public QueryController(DatabaseManager database_manager) {
        this.database_manager = database_manager;
    }

    public ArrayList<T> getData() throws SQLException {
        return getData(null);
    }

    public abstract ArrayList<T> getData(DatabaseObject key) throws SQLException;

    public abstract ArrayList<T> parseData(ResultSet result_set) throws SQLException;

}
