// Nicolas Abelanet
// Friday, October 22nd
// SearchByPublisherPanel.java

package assignment1;

import javax.swing.*;
import java.sql.SQLException;

public class SearchByPublisherPanel extends JPanel {

    public SearchByPublisherPanel(DatabaseManager database_manager) throws SQLException {

        PublisherQueryController publisher_query_controller = new PublisherQueryController(database_manager);
        BookQueryController book_query_controller = new BookQueryController(database_manager);
        PriceUpdateController price_update_controller = new PriceUpdateController();
        InventoryUpdateController inventory_update_controller = new InventoryUpdateController();

        ComboBoxController<Publisher> publisher_box_controller = new ComboBoxController<>(publisher_query_controller, true);
        ComboBoxController<Book> book_box_controller = new ComboBoxController<>(book_query_controller, false);
        TextAreaController<Price> price_controller = new TextAreaController<>(price_update_controller);
        ListController<Branch> inventory_controller = new ListController<>(inventory_update_controller);

        publisher_box_controller.addChild(book_box_controller);
        book_box_controller.addChild(price_controller);
        book_box_controller.addChild(inventory_controller);

        this.add(publisher_box_controller.getComboBox());
        this.add(book_box_controller.getComboBox());
        this.add(price_controller.getTextArea());
        this.add(inventory_controller.getList());

    }

}
