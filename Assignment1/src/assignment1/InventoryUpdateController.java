package assignment1;

public class InventoryUpdateController extends UpdateController<Branch[]> {

    public Branch[] handleUpdate(DatabaseObject update) throws Exception {

        if (update.getClass() == Book.class) {

            Book book = (Book) update;

            Object[] object_inventory = book.getInventory().toArray();
            Branch[] branch_inventory = new Branch[object_inventory.length];

            for (int i = 0; i < object_inventory.length; i++) {
                branch_inventory[i] = (Branch) object_inventory[i];
            }

            return branch_inventory;

        }

        throw new Exception("Handling update of type " + update.getClass().getSimpleName() + " is not implemented");

    }

}
