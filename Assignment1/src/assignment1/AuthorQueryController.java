// Nicolas Abelanet
// Friday, October 22nd
// AuthorQueryController.java

package assignment1;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AuthorQueryController extends QueryController <Author> {

    // The query for getting all authors.
    private static final String get_authors_query = "select AUTHOR_FIRST, AUTHOR_LAST, AUTHOR_NUM from HENRY_AUTHOR";

    public AuthorQueryController(DatabaseManager database_manager) {
        super(database_manager);
    }

    // Gets data from the database. This is a master controller so the key will always be null.
    @Override
    public ArrayList<Author> getData(DatabaseObject key) throws SQLException {
        return parseData(database_manager.executeQuery(get_authors_query));
    }

    // Parses the data received from the database.
    @Override
    public ArrayList<Author> parseData(ResultSet result_set) throws SQLException {
        return parseAuthors(result_set);
    }

    // Parses a result set into an array list of authors.
    private ArrayList<Author> parseAuthors(ResultSet result_set) throws SQLException {

        ArrayList<Author> authors = new ArrayList<>();

        // Gets the Author first name, last name, and number from the rows returned from the
        // database.
        while (result_set.next()) {
            String author_first = result_set.getString("AUTHOR_FIRST");
            String author_last = result_set.getString("AUTHOR_LAST");
            int author_num = result_set.getInt("AUTHOR_NUM");

            Author new_author = new Author(author_first, author_last, author_num);
            authors.add(new_author);
        }

        return authors;

    }




}
