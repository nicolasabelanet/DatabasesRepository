// Nicolas Abelanet
// Friday, October 22nd
// DataDisplayController.java

package assignment1;

import java.sql.SQLException;
import java.util.ArrayList;

public abstract class DataDisplayController {

    protected Boolean is_master; // Whether this data display controller is a master that only sends updates.
    protected ArrayList<DataDisplayController> children; // All the children that will receive this objects updates.

    public DataDisplayController(Boolean is_master) {
        this.is_master = is_master;
        Debug.log("Created DataDisplayController (type: " + this.getClass().getSimpleName() + ", is_master: " + this.is_master + ")");
        this.children = new ArrayList<>();
    }

    // Updates all the children.
    public void updateChildren (DatabaseObject update) throws SQLException  {
        for (DataDisplayController child : children) {

            if (update != null) {
                Debug.log(this.getClass().getSimpleName() + " sent an update '(" + update.getClass().getSimpleName() + ") " + update + "' to " + child.getClass().getSimpleName());
            }

            else {
                Debug.log(this.getClass().getSimpleName() + " sent an update (null) to " + child.getClass().getSimpleName());
            }

            child.update(update);

        }
    }

    public void addChild(DataDisplayController dc) {
        Debug.log(this.getClass().getSimpleName() + " added child " + dc.getClass().getSimpleName());
        children.add(dc);
    }

    public abstract void update (DatabaseObject update) throws SQLException;

}
