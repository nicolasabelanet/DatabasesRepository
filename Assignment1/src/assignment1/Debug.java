// Nicolas Abelanet
// Sunday, October 24th
// Debug.java

package assignment1;

public class Debug {

    public static void log (String log) {
        String method = Thread.currentThread().getStackTrace()[2].getMethodName();
        System.out.println("[" + method + "] " + log);
    }

}
