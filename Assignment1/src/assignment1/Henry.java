// Nicolas Abelanet
// Friday, October 22nd
// Henry.java

package assignment1;

import javax.swing.*;
import java.sql.SQLException;

public class Henry {

    public static void main(String[] args) {

        // The object that communicates directly with the database.
        DatabaseManager database_manager;

        try {
            database_manager = new DatabaseManager();

            JFrame jframe = new JFrame();
            jframe.setSize(750, 500);

            JTabbedPane tabs = new JTabbedPane();

            SearchByAuthorPanel search_by_author_panel = new SearchByAuthorPanel(database_manager);
            SearchByCategoryPanel search_by_category_panel = new SearchByCategoryPanel(database_manager);
            SearchByPublisherPanel search_by_publisher_panel = new SearchByPublisherPanel(database_manager);

            tabs.add("Search by Author", search_by_author_panel);
            tabs.add("Search by Category", search_by_category_panel);
            tabs.add("Search by Publisher", search_by_publisher_panel);

            jframe.add(tabs);
            jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jframe.setVisible(true);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

}
