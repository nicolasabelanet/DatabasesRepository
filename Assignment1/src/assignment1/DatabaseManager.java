// Nicolas Abelanet
// Friday, October 22nd
// DatabaseManager.java

package assignment1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

public class DatabaseManager {

    public static final String DB_URL = "jdbc:mysql://localhost/comp3421";
    public static final String USER = "root";
    public static final String PASS = "password";

    private final Connection connection;

    public DatabaseManager() throws SQLException {
       this.connection = DriverManager.getConnection(DB_URL, USER, PASS);
    }

    // The function responsible for executing generic queries.
    public ResultSet executeQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeQuery(query);
    }

}
