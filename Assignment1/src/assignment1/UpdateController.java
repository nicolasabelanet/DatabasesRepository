// Nicolas Abelanet
// Friday, October 22nd
// UpdateController.java

package assignment1;

import java.sql.SQLException;

public abstract class UpdateController<T> {
    // Handles an update given from a parent.
    public abstract T handleUpdate(DatabaseObject key) throws Exception;
}
