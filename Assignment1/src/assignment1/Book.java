// Nicolas Abelanet
// Friday, October 22nd
// Book.java

package assignment1;

import java.util.ArrayList;

public class Book extends DatabaseObject {

    private final String title; // The string full title of a book.
    private final Price price;  // The price of a book stored as a price object.
    private final ArrayList<Branch> inventory; // A list of branches representing a given books inventory.

    public Book(String title, Price price, ArrayList<Branch> inventory) {
        this.title = title;
        this.price = price;
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return title;
    }

    public Price getPrice () {
        return price;
    }

    public ArrayList<Branch> getInventory() {
        return  inventory;
    }

}
